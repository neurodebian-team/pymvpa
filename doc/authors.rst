.. -*- mode: rst -*-
.. ex: set sts=4 ts=4 sw=4 et tw=79:


The PyMVPA developers team currently consists of:

* `Michael Hanke`_, Dartmouth College, USA
* `Yaroslav O. Halchenko`_, Dartmouth College, USA
* `Per B. Sederberg`_, Princeton University, USA
* `Emanuele Olivetti`_, Fondazione Bruno Kessler, Italy

.. _Michael Hanke: http://apsy.gse.uni-magdeburg.de/hanke
.. _Yaroslav O. Halchenko: http://www.onerussian.com
.. _Per B. Sederberg: http://www.princeton.edu/~persed/
.. _Emanuele Olivetti: http://sra.fbk.eu/people/olivetti/


We are very grateful to the following people, who have contributed
valuable advice, code or documentation to PyMVPA:

* `Greg Detre`_, Princeton University, USA
* `Ingo Fründ`_, TU Berlin, Germany
* `Scott Gorlin`_, MIT, USA
* `Valentin Haenel`_, TU Berlin, Germany
* `James M. Hughes`_, Dartmouth College, USA
* `James Kyle`_, UCLA, USA
* `Tiziano Zito`_, BCCN, Germany

.. _Greg Detre: http://www.princeton.edu/~gdetre/
.. _James M. Hughes: http://www.cs.dartmouth.edu/~hughes/index.html
.. _Ingo Fründ: http://www.cognition.tu-berlin.de/menue/members/ingo_fruend/
.. _James Kyle: http://www.ccn.ucla.edu/users/jkyle
.. _Scott Gorlin: http://www.scottgorlin.com
.. _Valentin Haenel: http://www.cognition.tu-berlin.de/menue/members/valentin_haenel/
.. _Tiziano Zito: http://itb.biologie.hu-berlin.de/~zito/
